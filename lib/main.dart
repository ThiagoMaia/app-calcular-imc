import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MaterialApp(
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _msgStatus = 'Informe seus dados!';

  TextEditingController peso = TextEditingController();
  TextEditingController altura = TextEditingController();

  void _resetFields() {
    setState(() {
      _msgStatus = 'Informe seus dados!';
      _formKey.currentState.reset();
      peso.clear();
      altura.clear();
    });
  }

  void _calcular() {
    double imc = 0.0;
    double pesoAux = double.parse(peso.text);
    //convertendo valor de centímetro para metro.
    double alturaAux = double.parse(altura.text) / 100;

    setState(() {
      imc = pesoAux / (alturaAux * alturaAux);
      if (imc < 18.5) {
        _msgStatus =
            'Abaixo do Peso! \n' + 'IMC:' + '(${imc.toStringAsPrecision(3)})';
      } else if (imc >= 18.5 && imc < 24.9) {
        _msgStatus =
            'Peso Ideal! \n' + 'IMC:' + '(${imc.toStringAsPrecision(3)})';
      } else if (imc >= 25.0 && imc < 29.9) {
        _msgStatus = 'Levemente Acima do Peso! \n' +
            'IMC:' +
            '(${imc.toStringAsPrecision(3)})';
      } else if (imc >= 30.0 && imc < 34.9) {
        _msgStatus =
            'Obesidade Grau I! \n' + 'IMC:' + '(${imc.toStringAsPrecision(3)})';
      } else if (imc >= 35.0 && imc < 39.9) {
        _msgStatus = 'Obesidade Grau II! \n' +
            'IMC:' +
            '(${imc.toStringAsPrecision(3)})';
      } else if (imc > 40.0) {
        _msgStatus = 'Obesidade Grau III! \n' +
            'IMC:' +
            '(${imc.toStringAsPrecision(3)})';
      }
    });
    print('Valor calculado da altura: $alturaAux');
    print('Valor do peso: $pesoAux');
    print('Valor calculado do IMC: ${imc.toStringAsPrecision(3)}');
  }

  @override
  Widget build(BuildContext context) {
    /*O Scaffold é utilizado para facilitar a utilização
     dos componentes do Material Designer*/
    return Scaffold(
      appBar: AppBar(
        title: Text('Calculadora de IMC'),
        centerTitle: true,
        backgroundColor: Colors.green,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: _resetFields,
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        // adiciona scroll na tela
        padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
        child: Form(
          key: _formKey,
          child: Column(
            /*crossAxisAlignment alinha os elementos no centro e
            alargando os mesmos para as estremidades.*/
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Icon(Icons.person_outline, size: 120.0, color: Colors.green),
              TextFormField(
                maxLength: 4,
                keyboardType: TextInputType.number,
                inputFormatters: [
                  //apenas números
                  WhitelistingTextInputFormatter(RegExp("[0-9]")),
                  // podendo colocar outros também
                ],
                /*TextField*/
                decoration: InputDecoration(
                    /*Label*/
                    labelText: 'Peso (kg)',
                    labelStyle: TextStyle(color: Colors.green)),
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.green, fontSize: 25.0),
                controller: peso,
                //campo que recebe o valor do input
                validator: (value) {
                  //adicionando validações no input
                  return value.isEmpty ? 'Insira seu Peso.' : null;
                  //... podendo adicionar outras validações
                },
              ),
              TextFormField(
                maxLength: 4,
                keyboardType: TextInputType.number,
                inputFormatters: [
                  //apenas números
                  WhitelistingTextInputFormatter(RegExp("[0-9]")),
                  // podendo colocar outros também
                ],
                /*TextField*/
                decoration: InputDecoration(
                    /*Label*/
                    labelText: 'Altura (cm)',
                    labelStyle: TextStyle(color: Colors.green)),
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.green, fontSize: 25.0),
                controller: altura,
                //campo que recebe o valor do input
                validator: (value) {
                  //adicionando validações no input
                  return value.isEmpty ? 'Insira sua Altura.' : null;
                  //... podendo adicionar outras validações
                },
              ),
              Padding(
                padding: EdgeInsets.only(top: 40.0, bottom: 15.0),
                child: Container(
                  height: 50.0,
                  //aumenta a altura do button juntamente com o container
                  //padding: EdgeInsets.all(10.0),
                  child: RaisedButton(
                      onPressed: () {
                        //Só chama o método de calcular se passar pela validação dos campos.
                        if (_formKey.currentState.validate()) {
                          _calcular();
                        }
                      },
                      child: Text('Calcular',
                          style:
                              TextStyle(fontSize: 25.0, color: Colors.white)),
                      color: Colors.green),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(0.0),
                child: Text(
                  _msgStatus,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.green, fontSize: 25.0,
                    //fontWeight: FontWeight.bold
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
